@echo off
set count=1
FOR /F "skip=1 tokens=*" %%x in (dev\test_db.txt) DO (
    echo %count%     %%x
    set count+=1
)
set /p user-num=Select User: 
for /F "skip=%user-num% delims=" %%i in (dev\test_db.txt) do (
    set user=%%i
    goto nextline
)
:nextline
for /f "tokens=1,2 delims=_" %%a in ("%user%") do (
  git config --global user.name %%a
  git config --global user.email %%b
)